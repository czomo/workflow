FROM python:3.10-slim

LABEL maintainer="tomaszjdul@gmail.com"
WORKDIR /app
COPY requirements.txt .
RUN pip install --upgrade pip -r requirements.txt --no-cache-dir
RUN groupadd -r tdul && useradd --no-log-init -r -g tdul usertdul
USER usertdul
COPY main.py .
EXPOSE 8080

CMD [ "python", "./main.py"]
