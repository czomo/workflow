# devops recrutation task


## prerequisites
```
1. Copy the application code with requirements file into your code repository
 and share it with us.
```

## task requirements
```
1. Dockerize provided python app. Create the Dockerfile with every good practice you know about.
2. Create the CICD pipeline file on gitlab or any tool you know/want to know, to automate process of building and pushing image into any public docker repository.
3. Create a helm chart to deploy this application on a Kubernetes cluster - deployment + service + ingress.
4. Create argocd application manifest to run this app on the argocd cluster.
5. Add Dockerfile, CICD file, helm chart code, generated app manifests and argocd manifest into the repository.
```

## Folders and files description

- `app-manifest` > helm templated app manifest
- `helm` > helm source code
- `argocd` > argocd application manifest
- `Dockerfile` > simple small dockerfile
- `Dockerfile-multistage` > mustistage dockerfile manifest
- `docker-compose.yaml` > for local development
- `gitlab-ci.yaml` > pipelines configurrations

## Local development:
```
docker-compose up --build
curl localhost:8001
```

```
helm template helm/
helm upgrade --install helm helm/ -n tdul
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
kubectl port-forward svc/argocd-server -n argocd 8080:443

```
![](argo.png)